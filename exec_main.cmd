echo off
rem execute ./main.py on container from python3.7.6 images.
docker run -it --rm --name my-running-script -v %CD%:/usr/src/myapp -w /usr/src/myapp python:3.7.6 python main.py