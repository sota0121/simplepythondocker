# Develop on Docker with Official Python Image

## ref

[Docker Docs](https://hub.docker.com/_/python?tab=description)

## How to use this image ?

### Case1: Create a Dockerfile in your Python app project

Create dockerfile like as bellow

```dockerfile
FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python", "./your-daemon-or-script.py" ]
```

build and run the Docker Image

```bash
$ docker build -t my-python-app .
$ docker run -it --rm --name my-running-app my-python-app
```

### Case2: Run a single Python script(On Startup, use this case)

You don't have to make Dockerfile,  you can run a Python script by using the Python Docker image directly.

`$ docker run -it --rm --name my-running-script -v "$PWD":/usr/src/myapp -w /usr/src/myapp python:3 python your-daemon-or-script.py`

> NOTE : On Windows

Use %CD% instead of $PWD

`$ docker run -it --rm --name my-running-script -v %CD%:/usr/src/myapp -w /usr/src/myapp python:3 python your-daemon-or-script.py`
